<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\ZoneController;
use App\Http\Controllers\RetraitController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\RavaillementController;
use App\Http\Controllers\Humaine\PosteController;
use App\Http\Controllers\CategorieStockController;
use App\Http\Controllers\Finance\FinanceController;
use App\Http\Controllers\Humaine\EmployeController;
use App\Http\Controllers\Finance\OperationController;
use App\Http\Controllers\Params\DepartementController;
use App\Http\Controllers\Humaine\PrestataireController;
use App\Http\Controllers\Humaine\TypeContratController;
use App\Http\Controllers\Finance\TypeOperationController;
use App\Http\Controllers\Humaine\NiveauRemunerationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [AuthController::class, 'login'])->name('login');
Route::middleware('auth:sanctum')->post('logout', [AuthController::class,'logout'])->name('logout');

// Module PARAM
Route::middleware('auth:sanctum')->prefix('param')->group(function () {
    // ZONES
    Route::get('zones/all', [ZoneController::class, 'listeZone']);
    Route::resource('zones', ZoneController::class);

    // SITES
    Route::get('sites/all', [SiteController::class, 'listeSite']);
    Route::resource('sites', SiteController::class);

    // DEPARTEMENT
    Route::get('departements/liste', [DepartementController::class, 'listeAll']);
    Route::get('departements/site/{site}', [DepartementController::class, 'getDepartements']);
    Route::resource('departements', DepartementController::class);
});



// Module FINANCE
Route::middleware('auth:sanctum')->prefix('finance')->group(function () {

    // TYPE OPERATIONS
    Route::get('type-operations/entries', [TypeOperationController::class, 'listeEntries']);
    Route::get('type-operations/exits', [TypeOperationController::class, 'listeExits']);
    Route::resource('type-operations', TypeOperationController::class);

    // OPERATIONS
    Route::get('operations/entries', [OperationController::class, 'listEntries']);
    Route::get('operations/exits', [OperationController::class, 'listExits']);
    Route::get('operations/total', [OperationController::class, 'getTotalOperations']);
    Route::resource('operations', OperationController::class);

    // ROUTES DU TABLEAU BORD
    Route::prefix('dashboard')->group(function () {
        Route::post('montant', [FinanceController::class, 'getMontants']);
        Route::post('operations', [FinanceController::class, 'getOperations']);
        Route::post('operations-sortie', [FinanceController::class, 'pieChartOperationSorties']);
        Route::post('operations-entree', [FinanceController::class, 'pieChartOperationEntres']);
    });
});


// Module RESOURCE HUMAINE
Route::middleware('auth:sanctum')->prefix('humaine')->group(function () {

    // POSTES
    Route::get('postes/liste', [PosteController::class, 'listeAll']);
    Route::resource('postes', PosteController::class);

    // TYPE CONTRAT
    Route::get('type-contrat/liste', [TypeContratController::class, 'listeAll']);
    Route::resource('type-contrat', TypeContratController::class);

    // PRESTATAIRES
    Route::resource('prestataires', PrestataireController::class);
  
    // EMPLOYES
    Route::resource('employes', EmployeController::class);

    // NIVEAU REMUNERATION
    Route::get('niveau-remuneration/liste', [NiveauRemunerationController::class, 'listeAll']);
    Route::resource('niveau-remuneration', NiveauRemunerationController::class);

    // ROUTES DU TABLEAU BORD
    Route::prefix('dashboard')->group(function () {
        
    });
   
    // ROUTES DES STATISTIQUES
    Route::prefix('stats')->group(function () {
        Route::post('sexe', [EmployeController::class, 'graphSexe']);
        Route::post('age-sexe', [EmployeController::class, 'graphAgeSexe']);
        Route::post('fonction', [EmployeController::class, 'graphFonction']);
        Route::post('contrat', [EmployeController::class, 'graphContrat']);
        Route::post('departement', [EmployeController::class, 'graphDepartement']);
    });
});

// Module STOCK
Route::middleware('auth:sanctum')->prefix('stock')->group(function () {

    // CATEGORIES
    Route::get('categories/liste', [CategorieStockController::class, 'liste']);
    Route::get('sous-categories', [CategorieStockController::class, 'subCategories']);
    Route::resource('categories', CategorieStockController::class);
    Route::apiResource('ravitaillement', RavaillementController::class);
    Route::apiResource('sortie', RetraitController::class);
    Route::apiResource('commandes', CommandeController::class);


    Route::resource('items', ItemController::class);
    Route::get('items/liste', [ItemController::class, 'liste']);
   
    // ROUTES DES STATISTIQUES
    Route::prefix('stats')->group(function () {
        
    });
});
