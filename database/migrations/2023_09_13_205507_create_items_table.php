<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->longText('photo');
            $table->string('denomination');
            $table->string('description');
            $table->string('code');
            $table->foreignId('categorie_stock_id')->nullable()->constrained('categorie_stocks');
            $table->integer('prix');
            $table->integer('stock')->default(0);
            $table->enum('statut', [0, 1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }
};
