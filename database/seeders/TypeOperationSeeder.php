<?php

namespace Database\Seeders;

use App\Models\TypeOperation;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TypeOperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $types = [
            ['denomination' => 'Dépense', 'type' => 'Sortie'],
            ['denomination' => 'Revenu', 'type' => 'Entrée'],
            ['denomination' => 'Transfert interne', 'type' => 'Sortie'],
            ['denomination' => 'Vente', 'type' => 'Entrée'],
            ['denomination' => 'Frais généraux', 'type' => 'Sortie'],
            // Ajoutez d'autres exemples de types d'opération ici
        ];

        foreach ($types as $type) {
            TypeOperation::create($type);
        }
    }
}
