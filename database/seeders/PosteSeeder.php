<?php

namespace Database\Seeders;

use App\Models\Poste;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PosteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         // Exemple d'ajout de postes
         Poste::create([
            'titre' => 'Développeur Web',
            'description' => 'Responsable du développement des applications web.',
            'niveau_remuneration_id' => 1,
        ]);

        Poste::create([
            'titre' => 'Chef de Projet',
            'description' => 'Responsable de la gestion des projets et de l\'équipe.',
            'niveau_remuneration_id' => 2,
        ]);
    }
}
