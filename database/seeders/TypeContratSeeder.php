<?php

namespace Database\Seeders;

use App\Models\TypeContrat;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TypeContratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TypeContrat::create([
            'nom' => 'CDI',
            'description' => 'Contrat à durée indéterminée',
        ]);

        TypeContrat::create([
            'nom' => 'CDD',
            'description' => 'Contrat à durée déterminée',
        ]);
        
        TypeContrat::create([
            'nom' => 'Stage',
            'description' => 'Contrat à durée déterminée',
        ]);
    }
}
