<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'nom' => 'John',
            'prenom' => 'Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('azerty'),
            'role_id' => 1
        ]);


        User::create([
            'nom' => 'MARAT',
            'prenom' => 'Anthony',
            'email' => 'anthony.marat@yoboresto.com',
            'password' => bcrypt('azerty'),
            'role_id' => 1
        ]);
    
    }
}
