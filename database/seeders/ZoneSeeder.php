<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $zones = [
            ['denomination' => 'Estuaire', 'code' => 'G1'],
            ['denomination' => 'Haut-Ogooué', 'code' => 'G2'],
            ['denomination' => 'Moyen-Ogooué', 'code' => 'G3'],
            ['denomination' => 'Ngounié', 'code' => 'G4'],
            ['denomination' => 'Nyanga', 'code' => 'G5'],
            ['denomination' => 'Ogooué-Ivindo', 'code' => 'G6'],
            ['denomination' => 'Ogooué-Lolo', 'code' => 'G7'],
            ['denomination' => 'Ogooué-Maritime', 'code' => 'G8'],
            ['denomination' => 'Woleu-Ntem', 'code' => 'G9'],
        ];


        DB::table('zones')->insert($zones);
    }
}
