<?php

namespace Database\Seeders;

use App\Models\Prestataire;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PrestataireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Prestataire::create([
            'nom' => 'Prestataire 1',
            'type' => 'personne_physique',
            'email' => 'prestataire1@example.com',
            'telephone' => '123456789',
            'site_id' => 1, // Assurez-vous d'avoir un site avec l'ID 1 dans la table 'sites'
        ]);

        Prestataire::create([
            'nom' => 'Prestataire 2',
            'type' => 'personne_morale',
            'email' => 'prestataire2@example.com',
            'telephone' => '987654321',
            'site_id' => 2, // Assurez-vous d'avoir un site avec l'ID 2 dans la table 'sites'
        ]);
    }
}
