<?php

namespace Database\Seeders;

use App\Models\Employe;
use Illuminate\Database\Seeder;
use App\Models\DocumentsEmploye;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class EmployeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $employe1 = Employe::create([
            'photo' => '../assets/img/team/profile-picture-1.jpg',
            'nom' => 'John Doe',
            'prenom' => 'John',
            'email' => 'john@example.com',
            'date_naissance' => '1993-10-10',
            'poste_id' => 1,
            'type_contrat_id' => 1,
            'departement_id' => 1,
        ]);

        $employe2 = Employe::create([
            'photo' => '../assets/img/team/profile-picture-2.jpg',
            'nom' => 'Jane Smith',
            'prenom' => 'Jane',
            'email' => 'jane@example.com',
            'date_naissance' => '1993-10-10',
            'poste_id' => 2,
            'type_contrat_id' => 2,
            'departement_id' => 2,
        ]);
       
        $employe3 = Employe::create([
            'nom' => 'McCarty',
            'prenom' => 'Dave',
            'email' => 'dave@example.com',
            'date_naissance' => '1993-10-10',
            'poste_id' => 2,
            'type_contrat_id' => 2,
            'departement_id' => 2,
        ]);

        // Ajout de documents pour les employés
        $document1 = new DocumentsEmploye([
            'nom' => 'CV John Doe',
            'base64_data' => 'base64_encoded_data_here',
        ]);
        $employe1->documents()->save($document1);

        $document2 = new DocumentsEmploye([
            'nom' => 'CV Jane Smith',
            'base64_data' => 'base64_encoded_data_here',
        ]);
        $employe2->documents()->save($document2);
       
        $document3 = new DocumentsEmploye([
            'nom' => 'CV Jane Smith',
            'base64_data' => 'base64_encoded_data_here',
        ]);
        $employe3->documents()->save($document3);
        
    }
}
