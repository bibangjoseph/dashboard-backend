<?php

namespace Database\Seeders;

use App\Models\Departement;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DepartementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Departement::create([
            'denomination' => 'Comptabilité',
            'site_id' => 1, // ID du site auquel ce département appartient
        ]);
        Departement::create([
            'denomination' => 'Comptabilité',
            'site_id' => 2, // ID du site auquel ce département appartient
        ]);

        Departement::create([
            'denomination' => 'Logistique & Moyen',
            'site_id' => 1, // ID du site auquel ce département appartient
        ]);
        Departement::create([
            'denomination' => 'Logistique & Moyen',
            'site_id' => 2, // ID du site auquel ce département appartient
        ]);
    }
}
