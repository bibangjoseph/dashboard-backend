<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\NiveauRemuneration;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class NiveauRemunerationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        NiveauRemuneration::create([
            'nom' => 'Niveau 1',
            'min_salaire' => 25000,
            'max_salaire' => 35000,
        ]);

        NiveauRemuneration::create([
            'nom' => 'Niveau 2',
            'min_salaire' => 35001,
            'max_salaire' => 45000,
        ]);
    }
}
