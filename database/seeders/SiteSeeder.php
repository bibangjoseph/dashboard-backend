<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $sites = [
            // Estuaire
            ['zone_id' => 1, 'denomination' => 'YoboResto - Sablière', 'description' => 'Site de sablière à Libreville'],
            // Haut-Ogooué
            ['zone_id' => 2, 'denomination' => 'YoboResto - Franceville', 'description' => 'Site du carrefour Potoss 1 à Haut-Ogooué'],
            // Moyen-Ogooué
        ];

        foreach ($sites as $site) {
            DB::table('sites')->insert($site);
        }
    }
}
