<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\SiteSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\ZoneSeeder;
use Database\Seeders\DepartementSeeder;
use Database\Seeders\TypeContratSeeder;
use Database\Seeders\TypeOperationSeeder;
use Database\Seeders\NiveauRemunerationSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
       $this->call(ZoneSeeder::class);
       $this->call(SiteSeeder::class);
       $this->call(RoleSeeder::class);
       $this->call(UserSeeder::class);
       $this->call(TypeOperationSeeder::class);
       $this->call(DepartementSeeder::class);
       $this->call(TypeContratSeeder::class);
       $this->call(NiveauRemunerationSeeder::class);
       $this->call(PosteSeeder::class);
       $this->call(PrestataireSeeder::class);
       $this->call(EmployeSeeder::class);
    }
}
