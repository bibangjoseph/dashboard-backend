<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class MakeRepository extends Command
{
    protected $signature = 'make:repository {name}';
    protected $description = 'Create a new repository file';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $name = $this->argument('name');
        $repositoryFileName = app_path("Repositories/{$name}.php");

        // Vérifiez si le fichier du dépôt existe déjà
        if (File::exists($repositoryFileName)) {
            $this->error("Le fichier {$name}Repository existe déjà.");
            return;
        }

        // Générez le contenu du fichier du dépôt
        $content = "<?php\n\nnamespace App\Repositories;\n\nclass {$name}\n{\n    // Votre logique de dépôt ici\n}\n";

        // Créez le fichier du dépôt
        File::put($repositoryFileName, $content);

        $this->info("Le fichier {$name}Repository a été créé avec succès.");
    }
}
