<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class MakeControllerWithRepository extends Command
{
    protected $signature = 'make:controller-repo {name}';
    protected $description = 'Create a new controller with an associated repository';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $controllerName = $this->argument('name') . 'Controller';
        $repositoryName = $this->argument('name') . 'Repository';
        $requestName = $this->argument('name') . 'Request';
        $model = $this->argument('name');

        // Générer le contrôleur
        Artisan::call('make:controller', [
            'name' => $controllerName,
        ]);

        // Générer le fichier du dépôt
        Artisan::call('make:repository', [
            'name' => $repositoryName,
        ]);
       
        // Générer le fichier de la request
        Artisan::call('make:request', [
            'name' => $requestName,
        ]);

        // Obtenez le chemin du fichier du contrôleur
        $controllerFilePath = app_path('Http/Controllers/' . $controllerName . '.php');

        // Lire le modèle de contrôleur personnalisé
        $controllerTemplate = file_get_contents(resource_path('stubs/controller.stub'));

        // Remplacer les valeurs dans le modèle
        $controllerTemplate = str_replace('$namespace', 'App\Http\Controllers', $controllerTemplate);
        $controllerTemplate = str_replace('$repositoryName', $repositoryName, $controllerTemplate);
        $controllerTemplate = str_replace('$requestName', $requestName, $controllerTemplate);
        $controllerTemplate = str_replace('$class', $controllerName, $controllerTemplate);
        $controllerTemplate = str_replace('$repositoryVariable', lcfirst($repositoryName), $controllerTemplate);

        // Écrivez le contenu du modèle dans le fichier du contrôleur
        file_put_contents($controllerFilePath, $controllerTemplate);
       
       
        // Obtenez le chemin du fichier du contrôleur
        $repositoryFilePath = app_path('Repositories/' . $repositoryName . '.php');

        // Lire le modèle de contrôleur personnalisé
        $repositoryTemplate = file_get_contents(resource_path('stubs/repository.stub'));

        // Remplacer les valeurs dans le modèle
        $repositoryTemplate = str_replace('$namespace', 'App\Repositories', $repositoryTemplate);
        $repositoryTemplate = str_replace('$modelName', $model, $repositoryTemplate);
        $repositoryTemplate = str_replace('$class', $repositoryName, $repositoryTemplate);

        // Écrivez le contenu du modèle dans le fichier du contrôleur
        file_put_contents($repositoryFilePath, $repositoryTemplate);

        $this->info("Controller $controllerName and Repository $repositoryName created successfully with constructor.");
    }
}
