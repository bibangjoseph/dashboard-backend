<?php

namespace App\Models;

use App\Models\Site;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Prestataire extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'type', 'email', 'telephone', 'site_id'];

    public function site()
    {
        return $this->belongsTo(Site::class, 'site_id');
    }
}
