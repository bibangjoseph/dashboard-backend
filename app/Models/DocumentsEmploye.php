<?php

namespace App\Models;

use App\Models\Employe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DocumentsEmploye extends Model
{
    use HasFactory;

    protected $fillable = ['employe_id', 'nom', 'base64_data'];

    // Relation avec l'employé
    public function employe() {
        return $this->belongsTo(Employe::class);
    }
}
