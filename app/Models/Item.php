<?php

namespace App\Models;

use App\Models\CategorieStock;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'denomination',
        'photo',
        'code',
        'categorie_stock_id',
        'description',
        'prix',
    ];

    const STOCK = '1';
    const RUPTURE = '0';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->code = 'CODE-' . rand(1000, 9999);
        });
    }

    public function categorie()
    {
        return $this->belongsTo(CategorieStock::class, 'categorie_stock_id');
    }
}
