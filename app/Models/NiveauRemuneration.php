<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NiveauRemuneration extends Model
{
    use HasFactory;
    protected $fillable = ['nom', 'min_salaire', 'max_salaire'];
}
