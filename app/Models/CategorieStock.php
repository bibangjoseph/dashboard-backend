<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategorieStock extends Model
{
    use HasFactory;

    protected $fillable = ['denomination', 'parent_id'];


    public function sous_categories()
    {
        return $this->hasMany(CategorieStock::class, 'parent_id');
    }
}
