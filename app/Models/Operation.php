<?php

namespace App\Models;

use App\Models\Site;
use App\Models\User;
use App\Models\Departement;
use App\Models\TypeOperation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Operation extends Model
{
    use HasFactory;

    protected $fillable = [
        'titre',
        'description',
        'montant',
        'document',
        'type_id',
        'departement_id',
        'user_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($operation) {
            $operation->user_id = Auth::id(); // Associer l'ID de l'utilisateur authentifié à l'opération
        });
    }


    public function typeOperation()
    {
        return $this->belongsTo(TypeOperation::class, 'type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
   
    public function site()
    {
        return $this->belongsTo(Site::class, 'site_id');
    }
   
    public function departement()
    {
        return $this->belongsTo(Departement::class, 'departement_id');
    }
}
