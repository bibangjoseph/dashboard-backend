<?php

namespace App\Models;

use App\Models\Employe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TypeContrat extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'description'];

    // Relation avec les employés
    public function employes() {
        return $this->hasMany(Employe::class);
    }

}
