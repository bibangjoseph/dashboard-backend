<?php

namespace App\Models;

use App\Models\Operation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TypeOperation extends Model
{
    use HasFactory;

    const ENTREE = 'Entrée';
    const SORTIE = 'Sortie';

    protected $fillable = [
        'denomination',
        'type',
    ];
    
    public function operations()
    {
        return $this->hasMany(Operation::class, 'type_id');
    }
}
