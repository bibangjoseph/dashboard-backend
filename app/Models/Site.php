<?php

namespace App\Models;

use App\Models\Zone;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Site extends Model
{
    use HasFactory;

    protected $fillable = ['zone_id', 'denomination', 'description'];

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }
}
