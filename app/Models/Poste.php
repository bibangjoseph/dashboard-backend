<?php

namespace App\Models;

use App\Models\Employe;
use App\Models\NiveauRemuneration;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Poste extends Model
{
    use HasFactory;

    protected $fillable = ['titre', 'description', 'niveau_remuneration_id'];

    // Relation avec les employés
    public function employes() {
        return $this->hasMany(Employe::class);
    }
    
    // Relation avec les employés
    public function niveau_remuneration() {
        return $this->belongsTo(NiveauRemuneration::class);
    }


}
