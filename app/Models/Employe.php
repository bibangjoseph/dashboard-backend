<?php

namespace App\Models;

use App\Models\Poste;
use App\Models\Departement;
use App\Models\TypeContrat;
use App\Models\DocumentsEmploye;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employe extends Model
{
    use HasFactory;

    protected $fillable = ['photo', 'nom', 'prenom', 'email', 'sexe', 'poste_id', 'type_contrat_id', 'departement_id'];

    // Relation avec le poste
    public function poste() {
        return $this->belongsTo(Poste::class);
    }

    // Relation avec le département
    public function departement() {
        return $this->belongsTo(Departement::class);
    }

     // Relation avec les documents
    public function documents() {
        return $this->hasMany(DocumentsEmploye::class);
    }
     // Relation avec le type de contrat
    public function type_contrat() {
        return $this->belongsTo(TypeContrat::class);
    }


}
