<?php

namespace App\Models;

use App\Models\Site;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Zone extends Model
{
    use HasFactory;
    protected $fillable = ['denomination', 'code'];

    public function sites()
    {
        return $this->hasMany(Site::class);
    }
}
