<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommandeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'client' => 'required',
            'items' => 'required|array|min:1', // Au moins un article doit être inclus dans la commande
            'items.*.item_id' => 'required|exists:items,id', // ID de l'article
            'items.*.quantite' => 'required|integer|min:1',
        ];
    }
}
