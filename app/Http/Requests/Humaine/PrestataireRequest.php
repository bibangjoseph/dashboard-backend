<?php

namespace App\Http\Requests\Humaine;

use Illuminate\Foundation\Http\FormRequest;

class PrestataireRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nom' => 'required|string|max:255',
            'type' => 'required|in:personne_physique,personne_morale',
            'email' => 'required|string',
            'telephone' => 'required|string',
            'site_id' => 'required|exists:sites,id',
        ];
    }
}
