<?php

namespace App\Http\Requests\Humaine;

use Illuminate\Foundation\Http\FormRequest;

class PosteRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titre' => 'required|string|max:255',
            'description' => 'required|string',
            'niveau_remuneration_id' => 'required|exists:niveau_remunerations,id',
        ];
    }
}
