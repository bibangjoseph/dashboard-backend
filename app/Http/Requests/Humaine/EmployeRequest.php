<?php

namespace App\Http\Requests\Humaine;

use Illuminate\Foundation\Http\FormRequest;

class EmployeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'email' => 'required|email|unique:employes,email',
            'poste_id' => 'required|exists:postes,id',
            'contrat_type_id' => 'required|exists:posttype_contratses,id',
            'departement_id' => 'required|exists:departements,id',
        ];
    }
}
