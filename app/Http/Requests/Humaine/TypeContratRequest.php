<?php

namespace App\Http\Requests\Humaine;

use Illuminate\Foundation\Http\FormRequest;

class TypeContratRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nom' => 'required|string|max:255',
            'description' => 'required|string',
        ];
    }
}
