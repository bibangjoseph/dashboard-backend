<?php

namespace App\Http\Requests\Humaine;

use Illuminate\Foundation\Http\FormRequest;

class NiveauRemunerationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nom' => 'required|string|max:255',
            'min_salaire' => 'required|integer',
            'max_salaire' => 'required|integer',
        ];
    }

}
