<?php

namespace App\Http\Requests\Param;

use Illuminate\Foundation\Http\FormRequest;

class ZoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'denomination' => 'required|string|max:255',
            'code' => 'required|string|max:10',
        ];
    }
    
    public function messages()
    {
        return [
            'denomination.required' => 'Le champ "Dénomination" est requis.',
            'denomination.string' => 'Le champ "Dénomination" doit être une chaîne de caractères.',
            'denomination.max' => 'Le champ "Dénomination" ne doit pas dépasser :max caractères.',
            'code.required' => 'Le champ "Code" est requis.',
            'code.string' => 'Le champ "Code" doit être une chaîne de caractères.',
            'code.max' => 'Le champ "Code" ne doit pas dépasser :max caractères.',
        ];
    }
}
