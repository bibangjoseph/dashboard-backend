<?php

namespace App\Http\Requests\Finance;

use Illuminate\Foundation\Http\FormRequest;

class OperationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'titre' => 'required|string|max:255',
            'description' => 'nullable|string',
            'montant' => 'required|integer',
            'document' => 'nullable|string', // Peut-être validé davantage selon vos besoins
            'type_id' => 'required|exists:type_operations,id',
            'site_id' => 'required|exists:sites,id',
            'departement_id' => 'required|exists:departements,id',
        ];
    }

    public function messages()
    {
        return [
            'titre.required' => 'Le titre est requis.',
            'montant.required' => 'Le montant est requis.',
            'montant.integer' => 'Le montant doit être un nombre entier.',
            'type_id.required' => 'Le type d\'opération est requis.',
            'type_id.exists' => 'Le type d\'opération sélectionné n\'existe pas.',
            // Ajoutez d'autres messages personnalisés si nécessaire
        ];
    }
}
