<?php

namespace App\Http\Requests\Finance;

use Illuminate\Foundation\Http\FormRequest;

class TypeOperationRequest extends FormRequest
{
    public function rules()
    {
        return [
            'denomination' => 'required|string',
            'type' => 'required|in:Entrée,Sortie',
        ];
    }

    public function messages()
    {
        return [
            'denomination.required' => 'Le champ "Dénomination" est requis.',
            'type.required' => 'Le champ "Type" est requis.',
            'type.in' => 'La valeur du champ "Type" doit être soit "Entrée" soit "Sortie".',
        ];
    }
}
