<?php

namespace App\Http\Requests\Param;

use Illuminate\Foundation\Http\FormRequest;

class SiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    public function rules()
    {
        return [
            'zone_id' => 'required|exists:zones,id',
            'denomination' => 'required|string|max:255',
            'description' => 'nullable|string',
        ];
    }
}
