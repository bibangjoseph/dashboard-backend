<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use App\Http\Resources\PosteResource;
use App\Http\Resources\DepartementResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $photo = $this->photo ? $this->photo : strtoupper(substr($this->nom, 0, 1) . substr($this->prenom, 0, 1));
        return [
            'id' => $this->id,
            'initial' => $photo,
            'photo' => $this->photo,
            'nom' => $this->nom,
            'prenom' => $this->prenom,
            'email' => $this->email,
            'sexe' => $this->sexe,
            'poste' => $this->poste->only('id', 'titre'),
            'type_contrat' => $this->type_contrat->only('nom', 'id'),
            'departement' => $this->departement->only('id', 'denomination'),
            'documents' => $this->documents,
        ];
    }
}
