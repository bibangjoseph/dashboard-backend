<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'photo' => $this->photo,
            'denomination' => $this->denomination,
            'code' => $this->code,
            'statut' => $this->statut,
            'categorie' => $this->categorie,
            'prix' => $this->prix,
            'stock' => $this->stock,
        ];
    }
}
