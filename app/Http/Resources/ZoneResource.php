<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use App\Http\Resources\SiteResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ZoneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'denomination' => $this->denomination,
            'code' => $this->code,
            'sites' => SiteResource::collection($this->whenLoaded('sites')),
        ];
    }
}
