<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategorieStockRequest;
use App\Http\Resources\CategorieStockResource;
use App\Repositories\CategorieStockRepository;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class CategorieStockController extends Controller
{
    protected $categorieStockRepository;

    public function __construct(CategorieStockRepository $categorieStockRepository)
    {
        $this->categorieStockRepository = $categorieStockRepository;
    }

    public function index()
    {
        $data =  $this->categorieStockRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->categorieStockRepository->get();
        return $data;
    }

    public function subCategories()
    {
        $data =  $this->categorieStockRepository->subCategories();
        return $data;
    }


    public function store(CategorieStockRequest $request)
    {
        $data = $request->validated();
        $response =  $this->categorieStockRepository->store($data);
        if($response) {
            return $this->insertSuccess();
        }
        return $this->insertError();
    }

    // Reste du code du contrôleur...
}
