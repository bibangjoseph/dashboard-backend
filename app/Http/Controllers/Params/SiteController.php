<?php

namespace App\Http\Controllers\Params;

use App\Models\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SiteResource;
use App\Http\Requests\Param\SiteRequest;

class SiteController extends Controller
{
    public function index()
    {
        $sites = Site::with('zone')->paginate(10);
        return SiteResource::collection($sites);
    }
   
    public function listeSite()
    {
        $sites = Site::all();
        return SiteResource::collection($sites);
    }

    public function store(SiteRequest $request)
    {
        $site = Site::create($request->validated());
        if ($site) {
            return $this->insertSuccess(new SiteResource($site), 'Site créé avec succès', 201);
        }
        return $this->insertError();
    }

    public function show($id)
    {
        $site = Site::findOrFail($id);
        return $this->returnSuccess(new SiteResource($site));
    }

    public function update(SiteRequest $request, $id)
    {
        $site = Site::findOrFail($id);
        $updated = $site->update($request->validated());
        if ($updated) {
            return $this->updateSuccess(new SiteResource($site), 'Site mis à jour avec succès');
        }
        return $this->updateError();
    }

    public function destroy($id)
    {
        $deleted = Site::destroy($id);
        if ($deleted) {
            return $this->deleteSuccess('Site supprimé avec succès');
        }
        return $this->deleteError();
    }
}
