<?php

namespace App\Http\Controllers\Params;

use App\Models\Departement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DepartementResource;
use App\Http\Requests\Param\DepartementRequest;

class DepartementController extends Controller
{
    public function listeAll()
    {
        $departements = Departement::with('site')->get();
        return DepartementResource::collection($departements);
    }
    public function index()
    {
        $departements = Departement::with('site')->paginate(10);
        return DepartementResource::collection($departements);
    }

    public function store(DepartementRequest $request)
    {
        $departement = Departement::create($request->validated());
        if ($departement) {
            return $this->insertSuccess(new DepartementResource($departement), 'Département créé avec succès', 201);
        }
        return $this->insertError();
    }

    public function show($id)
    {
        $departement = Departement::findOrFail($id);
        return $this->returnSuccess(new DepartementResource($departement));
    }

    public function update(DepartementRequest $request, $id)
    {
        $departement = Departement::findOrFail($id);
        $updated = $departement->update($request->validated());
        if ($updated) {
            return $this->updateSuccess(new DepartementResource($departement), 'Département mis à jour avec succès');
        }
        return $this->updateError();
    }

    public function destroy($id)
    {
        $deleted = Departement::destroy($id);
        if ($deleted) {
            return $this->deleteSuccess('Département supprimé avec succès');
        }
        return $this->deleteError();
    }

    public function getDepartements($site)
    {
        $departements = Departement::where('site_id', $site)->get();
        return DepartementResource::collection($departements);
    }
}
