<?php

namespace App\Http\Controllers\Params;

use App\Models\Zone;
use App\Http\Controllers\Controller;
use App\Http\Resources\ZoneResource;
use App\Http\Requests\Param\ZoneRequest;

class ZoneController extends Controller
{
    public function index()
    {
        $zones = Zone::with(['sites'])->paginate(10);
        return ZoneResource::collection($zones);
    }

    public function listeZone()
    {
        $result =  Zone::with(['sites'])->get();
        $data = [];
        foreach ($result as $zone) {
            if($zone->sites->count() > 0) {
                $data[] = $zone;
            }
        }
        return ZoneResource::collection($data);
    }

    public function store(ZoneRequest $request)
    {
        $zone = Zone::create($request->validated());
        if($zone) {
            return $this->insertSuccess(new ZoneResource($zone), 'Zone créée avec succès', 201);
        }
        return $this->insertError();
    }

    public function show($id)
    {
        $zone = Zone::findOrFail($id);
        return new ZoneResource($zone);
    }

    public function update(ZoneRequest $request, $id)
    {
        $zone = Zone::findOrFail($id);
        $updated = $zone->update($request->validated());
        if ($updated) {
            return $this->updateSuccess(new ZoneResource($zone), 'Zone mise à jour avec succès');
        }
        return $this->updateError();
    }

    public function destroy($id)
    {
        $deleted = Zone::destroy($id);

        if ($deleted) {
            return $this->deleteSuccess();
        }
        return $this->deleteError();
    }

}
