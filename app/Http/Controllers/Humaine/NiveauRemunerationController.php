<?php

namespace App\Http\Controllers\Humaine;

use Illuminate\Http\Request;
use App\Models\NiveauRemuneration;
use App\Http\Controllers\Controller;
use App\Http\Resources\NiveauRemunerationResource;
use App\Http\Requests\Humaine\NiveauRemunerationRequest;

class NiveauRemunerationController extends Controller
{
    public function index()
    {
        $niveauxRemuneration = NiveauRemuneration::paginate(10);
        return NiveauRemunerationResource::collection($niveauxRemuneration);
    }

    public function show($id)
    {
        $niveauRemuneration = NiveauRemuneration::findOrFail($id);
        return new NiveauRemunerationResource($niveauRemuneration);
    }

    public function store(NiveauRemunerationRequest $request)
    {
        $niveauRemuneration = NiveauRemuneration::create([
            'nom' => $request->input('nom'),
            'min_salaire' => $request->input('min_salaire'),
            'max_salaire' => $request->input('max_salaire'),
        ]);

        if ($niveauRemuneration) {
            return $this->insertSuccess($niveauRemuneration, 'Niveau de rémunération créé avec succès');
        } else {
            return $this->insertError('Impossible de créer le niveau de rémunération');
        }
    }

    public function update(NiveauRemunerationRequest $request, $id)
    {
        $niveauRemuneration = NiveauRemuneration::find($id);

        if ($niveauRemuneration) {
            $updated = $niveauRemuneration->update([
                'nom' => $request->input('nom'),
                'min_salaire' => $request->input('min_salaire'),
                'max_salaire' => $request->input('max_salaire'),
            ]);

            if ($updated) {
                return $this->updateSuccess($niveauRemuneration, 'Niveau de rémunération mis à jour avec succès');
            } else {
                return $this->updateError('Impossible de mettre à jour le niveau de rémunération');
            }
        } else {
            return $this->notFoundError('Niveau de rémunération non trouvé');
        }
    }

    public function destroy($id)
    {
        $niveauRemuneration = NiveauRemuneration::findOrFail($id);

        if ($niveauRemuneration->delete()) {
            return $this->deleteSuccess('Niveau de rémunération supprimé avec succès');
        } else {
            return $this->deleteError('Impossible de supprimer le niveau de rémunération');
        }
    }

    public function listeAll()
    {
        $liste =  NiveauRemuneration::all();
        return NiveauRemunerationResource::collection($liste);
    }
}
