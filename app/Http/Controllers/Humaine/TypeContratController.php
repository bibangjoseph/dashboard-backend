<?php

namespace App\Http\Controllers\Humaine;

use App\Models\TypeContrat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TypeContratResource;
use App\Http\Requests\Humaine\TypeContratRequest;

class TypeContratController extends Controller
{
    public function store(TypeContratRequest $request)
    {
        $typeContrat = TypeContrat::create([
            'nom' => $request->input('nom'),
            'description' => $request->input('description'),
        ]);

        if ($typeContrat) {
            return $this->insertSuccess($typeContrat, 'Type de contrat créé avec succès');
        } else {
            return $this->insertError('Impossible de créer le type de contrat');
        }
    }

    public function update(TypeContratRequest $request, $id)
    {
        $typeContrat = TypeContrat::find($id);

        if ($typeContrat) {
            $updated = $typeContrat->update([
                'nom' => $request->input('nom'),
                'description' => $request->input('description'),
            ]);

            if ($updated) {
                return $this->updateSuccess($typeContrat, 'Type de contrat mis à jour avec succès');
            } else {
                return $this->updateError('Impossible de mettre à jour le type de contrat');
            }
        } else {
            return $this->notFoundError('Type de contrat non trouvé');
        }
    }

    public function destroy($id)
    {
        $typeContrat = TypeContrat::findOrFail($id);

        if ($typeContrat->delete()) {
            return $this->deleteSuccess('Type de contrat supprimé avec succès');
        } else {
            return $this->deleteError('Impossible de supprimer le type de contrat');
        }
    }

    public function index()
    {
        $typeContrats = TypeContrat::paginate(10);
        return TypeContratResource::collection($typeContrats);
    }
    public function listeAll()
    {
        $typeContrats = TypeContrat::all();
        return TypeContratResource::collection($typeContrats);
    }
}
