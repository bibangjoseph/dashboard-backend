<?php

namespace App\Http\Controllers\Humaine;

use App\Models\Prestataire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PrestataireResource;
use App\Http\Requests\Humaine\PrestataireRequest;

class PrestataireController extends Controller
{
    public function store(PrestataireRequest $request)
    {
        $prestataire = Prestataire::create([
            'nom' => $request->input('nom'),
            'type' => $request->input('type'),
            'email' => $request->input('email'),
            'telephone' => $request->input('telephone'),
            'site_id' => $request->input('site_id'),
        ]);

        if ($prestataire) {
            return $this->insertSuccess($prestataire, 'Prestataire créé avec succès');
        } else {
            return $this->insertError('Impossible de créer le prestataire');
        }
    }

    public function update(PrestataireRequest $request, $id)
    {
        $prestataire = Prestataire::find($id);

        if ($prestataire) {
            $updated = $prestataire->update([
                'nom' => $request->input('nom'),
                'type' => $request->input('type'),
                'email' => $request->input('email'),
                'telephone' => $request->input('telephone'),
                'telephone' => $request->input('telephone'),
                'site_id' => $request->input('site_id'),
            ]);

            if ($updated) {
                return $this->updateSuccess($prestataire, 'Prestataire mis à jour avec succès');
            } else {
                return $this->updateError('Impossible de mettre à jour le prestataire');
            }
        } else {
            return $this->notFoundError('Prestataire non trouvé');
        }
    }

    public function destroy($id)
    {
        $prestataire = Prestataire::findOrFail($id);

        if ($prestataire->delete()) {
            return $this->deleteSuccess('Prestataire supprimé avec succès');
        } else {
            return $this->deleteError('Impossible de supprimer le prestataire');
        }
    }

    public function index()
    {
        $prestataires = Prestataire::paginate(10);
        return PrestataireResource::collection($prestataires);
    }
}
