<?php

namespace App\Http\Controllers\Humaine;

use App\Models\Employe;
use Illuminate\Http\Request;
use App\Models\DocumentsEmploye;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeResource;
use App\Repositories\EmployeRepository;
use App\Http\Requests\Humaine\EmployeRequest;

class EmployeController extends Controller
{
    protected EmployeRepository $employeRepository;

    public function __construct(EmployeRepository $employeRepository)
    {
        $this->employeRepository = $employeRepository;
    }

    public function store(Request $request)
    {
        $data = (object) $request->all();
        $reponse = $this->employeRepository->store($data);
        if($reponse) {
            return $this->insertSuccess($request->all(), 'Employé créé avec succès');
        }
        return $this->insertError('Impossible de créer l\'employé');
    }

    public function update(EmployeRequest $request, $id)
    {
        $data = (object) $request->all();
        $reponse = $this->employeRepository->update($data, $id);
        if($reponse) {
            return $this->updateSuccess($request->all(), 'Employé mis à jour avec succès');
        }
        return $this->updateError('Impossible de mettre à jour l\'employé');
    }
    

    public function destroy($id)
    {
        $employe = Employe::findOrFail($id);

        if ($employe->delete()) {
            return $this->deleteSuccess('Employé supprimé avec succès');
        } else {
            return $this->deleteError('Impossible de supprimer l\'employé');
        }
    }

    public function index()
    {
        $employes = $this->employeRepository->listeEmployes();
        return $employes;
    }

    public function graphSexe(Request $request)
    {
        $data = (object) $request->all();
        return $this->employeRepository->graphSexe($data);
    }
   
    public function graphAgeSexe(Request $request)
    {
        $data = (object) $request->all();
        return $this->employeRepository->graphAgeSexe($data);
    }
    
    public function graphDepartement(Request $request)
    {
        $data = (object) $request->all();
        return $this->employeRepository->graphDepartement($data);
    }
    
    public function graphFonction(Request $request)
    {
        $data = (object) $request->all();
        return $this->employeRepository->graphFonction($data);
    }

    
    public function graphContrat(Request $request)
    {
        $data = (object) $request->all();
        return $this->employeRepository->graphContrat($data);
    }
}
