<?php

namespace App\Http\Controllers\Humaine;

use App\Models\Poste;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PosteResource;
use App\Http\Requests\Humaine\PosteRequest;

class PosteController extends Controller
{

    public function listeAll()
    {
        $postes = Poste::all();
        return PosteResource::collection($postes);
    }
    
    public function store(PosteRequest $request)
    {
        $poste = Poste::create([
            'titre' => $request->input('titre'),
            'description' => $request->input('description'),
            'niveau_remuneration_id' => $request->input('niveau_remuneration_id'),
        ]);

        if ($poste) {
            return $this->insertSuccess($poste, 'Poste créé avec succès');
        } else {
            return $this->insertError('Impossible de créer le poste');
        }
    }

    public function update(PosteRequest $request, $id)
    {
        $poste = Poste::find($id);

        if ($poste) {
            $updated = $poste->update([
                'titre' => $request->input('titre'),
                'description' => $request->input('description'),
                'niveau_remuneration_id' => $request->input('niveau_remuneration_id'),
            ]);

            if ($updated) {
                return $this->updateSuccess($poste, 'Poste mis à jour avec succès');
            } else {
                return $this->updateError('Impossible de mettre à jour le poste');
            }
        } else {
            return $this->notFoundError('Poste non trouvé');
        }
    }

    public function destroy($id)
    {
        $poste = Poste::findOrFail($id);

        if ($poste->delete()) {
            return $this->deleteSuccess('Poste supprimé avec succès');
        } else {
            return $this->deleteError('Impossible de supprimer le poste');
        }
    }

    public function index()
    {
        $postes = Poste::paginate(10);
        return PosteResource::collection($postes);
    }
}
