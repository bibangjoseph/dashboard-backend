<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\ItemResource;
use App\Repositories\ItemRepository;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class ItemController extends Controller
{
    protected $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function index()
    {
        $data =  $this->itemRepository->paginate();
        return ItemResource::collection($data);
    }
   
    public function liste()
    {
        $data =  $this->itemRepository->get();
        return $data;
    }


    public function store(ItemRequest $request)
    {
        $data = $request->validated();
        $response =  $this->itemRepository->store($data);
        if($response) {
            return $this->insertSuccess();
        }
        return $this->insertError();
    }

    // Reste du code du contrôleur...
}
