<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\DepartementRepository;
use App\Http\Requests\DepartementRequest;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class DepartementController extends Controller
{
    protected $departementRepository;

    public function __construct(DepartementRepository $departementRepository)
    {
        $this->departementRepository = $departementRepository;
    }

    public function index()
    {
        $data =  $this->departementRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->departementRepository->get();
        return $data;
    }


    public function store(DepartementRequest $request)
    {
        $data = $request->validated();
        $response =  $this->departementRepository->store($data);
        if($response) {
            return $this->returnSuccessInsert();
        }
        return $this->returnErrorInsert();
    }

    // Reste du code du contrôleur...
}