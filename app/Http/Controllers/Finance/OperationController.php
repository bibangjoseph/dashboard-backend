<?php

namespace App\Http\Controllers\Finance;

use Carbon\Carbon;
use App\Models\Operation;
use Illuminate\Http\Request;
use App\Models\TypeOperation;
use App\Http\Controllers\Controller;
use App\Http\Resources\OperationResource;
use App\Http\Requests\Finance\OperationRequest;

class OperationController extends Controller
{
    public function index()
    {
        $operations = Operation::with(['typeOperation', 'user'])->orderBy('id', 'desc')->paginate(10);
        return OperationResource::collection($operations);
    }

    public function store(OperationRequest $request)
    {
        $operation = Operation::create($request->validated());
        return $this->insertSuccess(new OperationResource($operation));
    }

    public function show(Operation $operation)
    {
        return new OperationResource($operation);
    }

    public function listEntries()
    {
        $entries = Operation::with(['user'])->whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::ENTREE);
        })->orderBy('id', 'desc')->paginate(10);

        return OperationResource::collection($entries);
    }

    public function listExits()
    {
        $exits = Operation::with(['user'])->whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::SORTIE);
        })->orderBy('id', 'desc')->paginate(10);

        return OperationResource::collection($exits);
    }

    public function getTotalOperations(Request $request)
    {
        $queryEntries = Operation::query();
        $queryExits = Operation::query();
        $query = Operation::query();
        if ($request->has('start_date') && $request->has('end_date')) {
            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date') . ' 00:00:00');
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date') . ' 23:59:59');
            $queryEntries->whereBetween('created_at', [$startDate, $endDate]);
            $queryExits->whereBetween('created_at', [$startDate, $endDate]);
            $query->whereBetween('created_at', [$startDate, $endDate]);
        }

        $totalExits = $queryExits->whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::SORTIE);
        })->sum('montant');
        
        $totalEntries = $queryEntries->whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::ENTREE);
        })->sum('montant');
    
        $operations = $query->with('typeOperation', 'user')->paginate(10); // Change 10 to your desired page size
        $responseData = [
            'total_entries' => $totalEntries,
            'total_exits' => $totalExits,
            'operations' => $operations
        ];
    
        return response()->json($responseData);
    }

    public function update(OperationRequest $request, Operation $operation)
    {
        $operation->update($request->validated());
        return $this->updateSuccess(new OperationResource($operation), 'Opération mise à jour avec succès');
    }

    public function destroy(Operation $operation)
    {
        $operation->delete();
        return $this->deleteSuccess('Opération supprimée avec succès');
    }
}
