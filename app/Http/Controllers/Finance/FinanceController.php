<?php

namespace App\Http\Controllers\Finance;

use DB;
use Carbon\Carbon;
use App\Models\Operation;
use Illuminate\Http\Request;
use App\Models\TypeOperation;
use App\Http\Controllers\Controller;
use App\Http\Resources\OperationResource;

class FinanceController extends Controller
{
    public function getMontants(Request $request)
    {
        $queryEntries = Operation::query();
        $queryExits = Operation::query();
        if ($request->has('start_date') && $request->has('end_date')) {
            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date') . ' 00:00:00');
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date') . ' 23:59:59');
            $queryEntries->whereBetween('created_at', [$startDate, $endDate]);
            $queryExits->whereBetween('created_at', [$startDate, $endDate]);
            $query->whereBetween('created_at', [$startDate, $endDate]);
        }

        $totalExits = $queryExits->whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::SORTIE);
        })->sum('montant');
        
        $totalEntries = $queryEntries->whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::ENTREE);
        })->sum('montant');
    
        $responseData = [
            'total_entries' => $totalEntries,
            'total_exits' => $totalExits,
            'solde' => intval($totalEntries) - intval($totalExits)
        ];
    
        return response()->json($responseData);
    }



    public function getOperations(Request $request)
    {
        $operations = Operation::with(['user','typeOperation'])->orderBy('id', 'desc')->limit(10)->get();
        return $operations;
    }


    public function pieChartOperationSorties(Request $request)
    {
        $totalExitsByType = Operation::whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::SORTIE);
        })
        ->join('type_operations', 'operations.type_id', '=', 'type_operations.id')
        ->select('type_operations.denomination as denomination', \DB::raw('SUM(operations.montant) as montant'))
        ->groupBy('denomination')
        ->get();
        return $totalExitsByType;
    }
   
    public function pieChartOperationEntres(Request $request)
    {
        $totalExitsByType = Operation::whereHas('typeOperation', function ($query) {
            $query->where('type', TypeOperation::ENTREE);
        })
        ->join('type_operations', 'operations.type_id', '=', 'type_operations.id')
        ->select('type_operations.denomination as denomination', \DB::raw('SUM(operations.montant) as montant'))
        ->groupBy('denomination')
        ->get();
        return $totalExitsByType;
    }
}
