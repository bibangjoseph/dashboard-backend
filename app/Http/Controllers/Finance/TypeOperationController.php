<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Models\TypeOperation;
use App\Http\Controllers\Controller;
use App\Http\Resources\TypeOperationResource;
use App\Http\Requests\Finance\TypeOperationRequest;

class TypeOperationController extends Controller
{

    public function store(TypeOperationRequest $request)
    {
        $typeOperation = TypeOperation::create([
            'denomination' => $request->input('denomination'),
            'type' => $request->input('type'),
        ]);

        if ($typeOperation) {
            return $this->insertSuccess($typeOperation, 'Type d\'opération créé avec succès');
        } else {
            return $this->insertError('Impossible de créer le type d\'opération');
        }
    }

    public function update(TypeOperationRequest $request, $id)
    {
        
        $typeOperation = TypeOperation::find($id);

        if ($typeOperation) {
            $updated = $typeOperation->update([
                'denomination' => $request->input('denomination'),
                'type' => $request->input('type'),
            ]);
        
            if ($updated) {
                return $this->updateSuccess($typeOperation, 'Type d\'opération mis à jour avec succès');
            } else {
                return $this->updateError('Impossible de mettre à jour le type d\'opération');
            }
        } else {
            return $this->notFoundError('Type d\'opération non trouvé');
        }
        
    }

    public function destroy($id)
    {
        $typeOperation = TypeOperation::findOrFail($id);

        if ($typeOperation->delete()) {
            return $this->deleteSuccess('Type d\'opération supprimé avec succès');
        } else {
            return $this->deleteError('Impossible de supprimer le type d\'opération');
        }
    }
   
    public function index()
    {
        $types = TypeOperation::paginate(10); // Par exemple, paginer par 10 éléments par page
        return TypeOperationResource::collection($types);
    }

    public function listeEntries()
    {
        $types = TypeOperation::where('type', TypeOperation::ENTREE)->get();
        return TypeOperationResource::collection($types);
    }
    
    public function listeExits()
    {
        $types = TypeOperation::where('type', TypeOperation::SORTIE)->get();
        return TypeOperationResource::collection($types);
    }
}
