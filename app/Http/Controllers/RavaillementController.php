<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RavaillementRepository;
use App\Http\Requests\RavaillementRequest;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class RavaillementController extends Controller
{
    protected $ravaillementRepository;

    public function __construct(RavaillementRepository $ravaillementRepository)
    {
        $this->ravaillementRepository = $ravaillementRepository;
    }

    public function index()
    {
        $data =  $this->ravaillementRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->ravaillementRepository->get();
        return $data;
    }


    public function store(RavaillementRequest $request)
    {
        $data = $request->validated();
        $response =  $this->ravaillementRepository->store($data);
        if($response) {
            return $this->insertSuccess();
        }
        return $this->insertError();
    }

    // Reste du code du contrôleur...
}