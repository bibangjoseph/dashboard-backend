<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CommandeRepository;
use App\Http\Requests\CommandeRequest;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class CommandeController extends Controller
{
    protected $commandeRepository;

    public function __construct(CommandeRepository $commandeRepository)
    {
        $this->commandeRepository = $commandeRepository;
    }

    public function index()
    {
        $data =  $this->commandeRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->commandeRepository->get();
        return $data;
    }


    public function store(CommandeRequest $request)
    {
        $data = $request->validated();
        $response =  $this->commandeRepository->store($data);
        if($response) {
            return $this->insertSuccess();
        }
        return $this->insertError();
    }

    // Reste du code du contrôleur...
}