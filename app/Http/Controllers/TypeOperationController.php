<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TypeOperationRepository;
use App\Http\Requests\TypeOperationRequest;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class TypeOperationController extends Controller
{
    protected $typeOperationRepository;

    public function __construct(TypeOperationRepository $typeOperationRepository)
    {
        $this->typeOperationRepository = $typeOperationRepository;
    }

    public function index()
    {
        $data =  $this->typeOperationRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->typeOperationRepository->get();
        return $data;
    }


    public function store(TypeOperationRequest $request)
    {
        $data = $request->validated();
        $response =  $this->typeOperationRepository->store($data);
        if($response) {
            return $this->returnSuccessInsert();
        }
        return $this->returnErrorInsert();
    }

    // Reste du code du contrôleur...
}