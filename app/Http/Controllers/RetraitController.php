<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RetraitRepository;
use App\Http\Requests\RetraitRequest;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class RetraitController extends Controller
{
    protected $retraitRepository;

    public function __construct(RetraitRepository $retraitRepository)
    {
        $this->retraitRepository = $retraitRepository;
    }

    public function index()
    {
        $data =  $this->retraitRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->retraitRepository->get();
        return $data;
    }


    public function store(RetraitRequest $request)
    {
        $data = $request->validated();
        $response =  $this->retraitRepository->store($data);
        if($response === true) {
            return $this->insertSuccess();
        }
        if(gettype($response) === 'string') {
            return $this->insertError($response);
        }
        return $this->insertError();
        
    }

    // Reste du code du contrôleur...
}