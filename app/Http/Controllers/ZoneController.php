<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ZoneRepository;
use App\Http\Requests\ZoneRequest;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class ZoneController extends Controller
{
    protected $zoneRepository;

    public function __construct(ZoneRepository $zoneRepository)
    {
        $this->zoneRepository = $zoneRepository;
    }

    public function index()
    {
        $data =  $this->zoneRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->zoneRepository->get();
        return $data;
    }


    public function store(ZoneRequest $request)
    {
        $data = $request->validated();
        $response =  $this->zoneRepository->store($data);
        if($response) {
            return $this->returnSuccessInsert();
        }
        return $this->returnErrorInsert();
    }

    // Reste du code du contrôleur...
}