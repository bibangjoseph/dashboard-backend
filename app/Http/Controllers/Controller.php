<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected function insertSuccess($data = null, $message = 'Enregistrement effectué avec succès')
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ], Response::HTTP_CREATED);
    }

    protected function insertError($message = 'Échec de l\'enregistrement')
    {
        return response()->json([
            'success' => false,
            'message' => $message,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
   
    protected function updateError($message = 'Échec de la mise à jour')
    {
        return response()->json([
            'success' => false,
            'message' => $message,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    // Vous pouvez ajouter d'autres méthodes de réponse ici

    // Exemple de méthode pour gérer les réponses de mise à jour réussies
    protected function updateSuccess($data = null, $message = 'Mise à jour effectuée avec succès')
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ], Response::HTTP_OK);
    }


    protected function deleteSuccess($message = 'Suppression effectuée avec succès')
    {
        return response()->json([
            'success' => true,
            'message' => $message,
        ], Response::HTTP_OK);
    }
   
    protected function deleteError($message = 'Échec de la suppression')
    {
        return response()->json([
            'success' => false,
            'message' => $message,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
