<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SiteRepository;
use App\Http\Requests\SiteRequest;


/**
 * --------------------------------------------------------------------------
 *                      Controleur App\Http\Controllers
 * --------------------------------------------------------------------------
 *
 * @category Controller
 * @package  Packages\App\Http\Controllers
 * @author   Joseph Donovan BIBANG BEFENE <bibangjose@gmail.com>
 * @link     https://laravel.com/docs/10.x/controllers
 */

class SiteController extends Controller
{
    protected $siteRepository;

    public function __construct(SiteRepository $siteRepository)
    {
        $this->siteRepository = $siteRepository;
    }

    public function index()
    {
        $data =  $this->siteRepository->paginate();
        return $data;
    }
   
    public function liste()
    {
        $data =  $this->siteRepository->get();
        return $data;
    }


    public function store(SiteRequest $request)
    {
        $data = $request->validated();
        $response =  $this->siteRepository->store($data);
        if($response) {
            return $this->returnSuccessInsert();
        }
        return $this->returnErrorInsert();
    }

    // Reste du code du contrôleur...
}