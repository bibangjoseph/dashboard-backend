<?php

namespace App\Repositories;

use App\Models\Item;

class ItemRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $model = new Item;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = Item::paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = Item::all();
        return $data;
    }
}