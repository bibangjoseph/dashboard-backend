<?php

namespace App\Repositories;

use App\Models\Zone;

class ZoneRepository
{
    
    public function store($data)
    {
        $model = new Zone;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = Zone::paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = Zone::all();
        return $data;
    }
}
