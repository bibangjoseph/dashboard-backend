<?php

namespace App\Repositories;

use App\Models\Site;

class SiteRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $model = new Site;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = Site::with(['zone'])->paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = Site::all();
        return $data;
    }
}
