<?php

namespace App\Repositories;

use App\Models\Item;
use App\Models\Ravitaillement;

class RavaillementRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $model = new Ravitaillement;
        $model->fill($data);
        if($model->save()) {
            $article = Item::find($data['item_id']);
            if ($article) {
                $article->increment('stock', $data['quantite']);
                if ($article->stock > 0) {
                    $article->statut = Item::STOCK; // En stock
                } else {
                    $article->statut = Item::RUPTURE;// Épuisé
                }
               
                $article->save();
            }
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = Ravitaillement::paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = Ravitaillement::all();
        return $data;
    }
}