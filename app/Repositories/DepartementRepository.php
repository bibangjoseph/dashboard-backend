<?php

namespace App\Repositories;

use App\Models\Departement;

class DepartementRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $model = new Departement;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = Departement::paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = Departement::all();
        return $data;
    }
}