<?php

namespace App\Repositories;

use App\Models\TypeOperation;

class TypeOperationRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $model = new TypeOperation;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = TypeOperation::paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = TypeOperation::all();
        return $data;
    }
}