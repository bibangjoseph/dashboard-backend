<?php

namespace App\Repositories;

use App\Models\Type;

class TypeRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $model = new Type;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = Type::paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = Type::all();
        return $data;
    }
}