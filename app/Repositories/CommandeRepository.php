<?php

namespace App\Repositories;

use App\Models\Commande;

class CommandeRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {

        dd($data);
        $model = new Commande;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $data = Commande::paginate(10);
        return $data;
    }
    
    
    public function get()
    {
        $data = Commande::all();
        return $data;
    }
}