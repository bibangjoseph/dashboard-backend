<?php
namespace App\Repositories;

use App\Models\Poste;
use App\Models\Employe;
use App\Models\Departement;
use App\Models\TypeContrat;
use Illuminate\Http\Request;
use App\Models\DocumentsEmploye;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\EmployeResource;

class EmployeRepository
{
    public function store($data)
    {
        $employeData = [
            'nom' => $data->nom,
            'photo' => $data->photo,
            'prenom' => $data->prenom,
            'sexe' => $data->sexe,
            'date_naissance' => $data->date_naissance,
            'email' => $data->email,
            'poste_id' => $data->poste_id,
            'type_contrat_id' => $data->type_contrat_id,
            'departement_id' => $data->departement_id,
        ];
        $employe = Employe::create($employeData);
        if ($employe) {
            $documents = $data->documents;
            if (!empty($documents)) {
                foreach ($documents as $documentData) {
                    $document = new DocumentsEmploye([
                        'nom' => $documentData['nom'],
                        'base64_data' => $documentData['base64_data'],
                    ]);
                    $employe->documents()->save($document);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $employe = Employe::find($id);
        if ($employe) {
            $employeData = [
                'nom' => $data->nom,
                'photo' => $data->photo,
                'prenom' => $data->prenom,
                'email' => $data->email,
                'sexe' => $data->sexe,
                'date_naissance' => $data->date_naissance,
                'poste_id' => $data->poste_id,
                'type_contrat_id' => $data->type_contrat_id,
                'departement_id' => $data->departement_id,
            ];
            $updated = $employe->update($employeData);
            if ($updated) {
                // Gérer la mise à jour des documents
                $documents = $data->documents;
    
                if (!empty($documents)) {
                    foreach ($documents as $documentData) {
                        if (isset($documentData['id'])) {
                            $document = DocumentsEmploye::find($documentData['id']);
    
                            if ($document) {
                                $document->update([
                                    'nom' => $documentData['nom'],
                                    'base64_data' => $documentData['base64_data'],
                                ]);
                            }
                        } else {
                            $document = new DocumentsEmploye([
                                'nom' => $documentData['nom'],
                                'base64_data' => $documentData['base64_data'],
                            ]);
    
                            $employe->documents()->save($document);
                        }
                    }
                }
                return true;
            } 
            return false;
        } else {
            return false;
        }
    }

    public function listeEmployes()
    {
        $employes = Employe::orderBy('id', 'desc')->paginate(10);
        return EmployeResource::collection($employes);
    }

    public function graphSexe($conditions)
    {
        $hommes = Employe::where('sexe', 'Homme')->count();
        $femmes = Employe::where('sexe', 'Femme')->count();
        return array($hommes, $femmes);
    }

    public function graphAgeSexe($conditions)
    {
        // Créez une liste de toutes les tranches d'âge possibles
        $tranchesAgePossibles = ['18-24', '25-34', '35-44', '45-54', '55-64', '65+'];

        // Exécutez la requête pour obtenir le nombre d'employés par sexe et par tranche d'âge
        $results = DB::table('employes')
            ->select(
                DB::raw('CASE
                    WHEN TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()) BETWEEN 18 AND 24 THEN "18-24"
                    WHEN TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()) BETWEEN 25 AND 34 THEN "25-34"
                    WHEN TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()) BETWEEN 35 AND 44 THEN "35-44"
                    WHEN TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()) BETWEEN 45 AND 54 THEN "45-54"
                    WHEN TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()) BETWEEN 55 AND 64 THEN "55-64"
                    ELSE "65+"
                END AS tranche_age'),
                'sexe',
                DB::raw('COUNT(*) as nombre_employes')
            )
            ->groupBy('sexe', 'tranche_age')
            ->orderBy('sexe', 'asc')
            ->orderBy('tranche_age', 'asc')
            ->get();

        // Créez trois tableaux distincts pour les tranches d'âge, les hommes et les femmes
        $tranches = [];
        $hommes = [];
        $femmes = [];

        // Remplissez les tableaux avec les données appropriées
        foreach ($tranchesAgePossibles as $trancheAge) {
            // Tableau des tranches d'âge
            $tranches[] = $trancheAge;
            // Tableau des hommes
            $hommes[] = $results->where('sexe', 'Homme')->where('tranche_age', $trancheAge)->sum('nombre_employes');
            // Tableau des femmes
            $femmes[] = $results->where('sexe', 'Femme')->where('tranche_age', $trancheAge)->sum('nombre_employes');
        }

        return [$tranches, $hommes, $femmes];
    }

    public function graphDepartement($conditions)
    {
        $departements = Departement::all();       
        $data['labels'] = [];
        $data['data'] = [];
        foreach ($departements as $depart) {
            $count = Employe::where('departement_id', $depart->id)->count();
            if($count > 0) {
                $data['labels'][] = $depart->denomination . ', ' . $depart->site->denomination;
                $data['data'][] = $count;
            }
        }
        return $data;        
    }
    
    public function graphFonction($conditions)
    {
        $postes = Poste::all();       
        $data['labels'] = [];
        $data['data'] = [];
        foreach ($postes as $depart) {
            $count = Employe::where('poste_id', $depart->id)->count();
            if($count > 0) {
                $data['labels'][] = $depart->titre;
                $data['data'][] = $count;
            }
        }
        return $data;        
    }
    
    public function graphContrat($conditions)
    {
        $typeContrat = TypeContrat::all();       
        $data['labels'] = [];
        $data['data'] = [];
        foreach ($typeContrat as $type) {
            $count = Employe::where('type_contrat_id', $type->id)->count();
            if($count > 0) {
                $data['labels'][] = $type->nom;
                $data['data'][] = $count;
            }
        }
        return $data;        
    }
}
