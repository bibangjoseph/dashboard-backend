<?php

namespace App\Repositories;

use App\Models\CategorieStock;

class CategorieStockRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $model = new CategorieStock;
        $model->fill($data);
        if($model->save()) {
            return true;
        }
        return false;
    }

    public function paginate()
    {
        $categories = CategorieStock::whereNull('parent_id')
        ->with(['sous_categories' => function ($query) {
            $query->paginate(10);
        }])
        ->paginate(10);

        return $categories;
    }
    
    
    public function get()
    {
        $data = CategorieStock::whereNull('parent_id')->get();
        return $data;
    }
    
    public function subCategories()
    {
        $data = CategorieStock::where('parent_id', '<>', null)->get();
        return $data;
    }
}
