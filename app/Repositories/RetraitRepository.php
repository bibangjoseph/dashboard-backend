<?php

namespace App\Repositories;

use App\Models\Item;
use App\Models\Retrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\RetraitResource;


class RetraitRepository
{
    // Votre logique de dépôt ici

    public function store($data)
    {
        $user = Auth::user();
        if ($user) {
            $data['user_id'] = $user->id;
            $article = Item::find($data['item_id']);
            if ($article->stock >= $data['quantite']) {
                $article->decrement('stock', $data['quantite']);
                if ($article->stock > 0) {
                    $article->statut = Item::STOCK; // En stock
                } else {
                    $article->statut = Item::RUPTURE; // Épuisé
                }
                $model = new Retrait;
                $model->fill($data);
                if($article->save()) {
                    $model->save();
                    return true;
                }
            }
            return 'Stock non disponible';
        } 
        return false;
    }

    public function paginate()
    {
        $data = Retrait::paginate(10);
        return RetraitResource::collection($data);
    }
    
    
    public function get()
    {
        $data = Retrait::all();
        return $data;
    }
}